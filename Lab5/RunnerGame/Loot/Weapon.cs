﻿using System.Runtime.Serialization;

namespace Hunting.Loot
{
    [DataContract]
    public class Weapon
    {
        [DataMember]
        public int ActionTime { get; set; }
        [DataMember]
        public WeaponType TypeOfWeapon { get; set; }

        public Weapon(int actionTime, WeaponType typeOfweapon)
        {
            ActionTime = actionTime;
            TypeOfWeapon = typeOfweapon;
        }
    }
}
