﻿using System.Runtime.Serialization;

namespace Hunting.Loot
{
    [DataContract]
    public enum WeaponType
    {
        [EnumMember]
        Glock,
        [EnumMember]
        AK_47,
        [EnumMember]
        Rifle,
        [EnumMember]
        M4A1,
    }
}
