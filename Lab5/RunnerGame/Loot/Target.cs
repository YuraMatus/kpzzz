﻿using System.Runtime.Serialization;

namespace Hunting.Loot
{
    [DataContract]
    public class Target
    {
        [DataMember]
        public Position TargetPosition { get; set; }
        [DataMember]
        public int Value { get; set; }

        public Target(Position tPosition, int value)
        {
            TargetPosition = tPosition;
            Value = value;
        }
    }
}
