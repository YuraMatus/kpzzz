﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using Hunting.Loot;
using Hunting.HunterPlayer;

namespace Hunting
{
    [DataContract]
    public class Game
    {
        [DataMember]
        public List<Target> targets { get; private set; }
        [DataMember]
        public List<Weapon> weapons { get; private set; }
        [DataMember]
        public Hunter one_of_hunter { get; set; }

        public Game(Hunter _hunter):this()
        {
            one_of_hunter = _hunter;
        }

        public Game()
        {
            targets = new List<Target>()
            {
                new Target(new Position(10, 80), 100),
                new Target(new Position(10, 100), 200),
                new Target(new Position(10, 110), 300),
            };

            weapons = new List<Weapon>()
            {
                new Weapon(200, WeaponType.AK_47),
                new Weapon(250, WeaponType.Glock),
                new Weapon(300, WeaponType.M4A1),
                new Weapon(350, WeaponType.Rifle),
            };

            one_of_hunter = new Hunter("Yura","Glock",0);
        }


        public static string DataPath = "C:\\Users\\HP\\Desktop\\КПЗ\\KPZ_lab5\\hunter.dat";

        public static Game LoadGame()
        {
            return File.Exists(DataPath) ? DataSerializer.DeserializeData(DataPath) : new Game();
        }

        public void Save()
        {
            DataSerializer.SerializeData(DataPath,this);
        }
    }
}
