﻿using System.Runtime.Serialization;

namespace Hunting.HunterPlayer
{
    [DataContract]
    public class Hunter
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Weapon { get; set; }
        [DataMember]
        public int Score { get; set; }
        [DataMember]
        public Position HunterPosition { get; set; }
        public Hunter()
        {
            HunterPosition = new Position(0,0);
        }
        public Hunter(string _name,string _weapon, int _score):this()
        {
            Name = _name;
            Weapon = _weapon;
            Score = _score;
        }

        public override string ToString()
        {
            return $"Name: {Name}, Weapon: {Weapon}, Score: {Score}, Position: {HunterPosition}";
        }
    }
}
