﻿using System;
using System.Collections.Generic;
using Robot.Common;
using System.Linq;


namespace Yurii.Matus.RobotChallange
{
    public class Matus_Algo : IRobotAlgorithm
    {
        public string Author
        {
            get
            {
                return "Yurii Matus";
            }
        }

        public Dictionary<int, Position> indexToPosition;

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot currentRobot = robots[robotToMoveIndex];

            var myBots = robots
                .Select(r => r)
                .Where(rb => rb.OwnerName == "Yurii Matus");


            if (myBots.Count() > indexToPosition.Count)
            {
                indexToPosition.Add(robots.IndexOf(myBots.Last()), new Position(-1, -1));
            }

            //Collect Energy
            if (indexToPosition[robotToMoveIndex].X<=4)
            {
                return new CollectEnergyCommand();
            }
            else //Move robot
            {
                var closestStation = map.Stations
                    .OrderByDescending(r => r.RecoveryRate)
                    .ThenBy(t => FindDistance(currentRobot.Position, t.Position))
                    .TakeWhile(t => FindDistance(currentRobot.Position, t.Position) < (int)(currentRobot.Energy * 5))
                    .First();

                Position wantedPosition = closestStation.Position;
                if (currentRobot.Energy < 1)
                {
                    return new MoveCommand() { NewPosition = currentRobot.Position };
                }
                int neededEnergy = FindDistance(currentRobot.Position, wantedPosition);
                while (neededEnergy > currentRobot.Energy * 0.5)
                {
                    neededEnergy = FindDistance(currentRobot.Position, wantedPosition);
                }
                return new MoveCommand() { NewPosition = wantedPosition };
            }
        }
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
    }
}
