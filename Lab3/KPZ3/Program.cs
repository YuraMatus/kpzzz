﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ3
{
    public class CRecords : IComparable<CRecords>
    {
        public int nKilling { get; set; }
        public int nId { get; set; }
        public string sWeapon { get; set; }
        public string sLocation { get; set; }
        public int CompareTo(CRecords killings)
        {
            if (nKilling > killings.nKilling)
                return 1;
            else if (nKilling < killings.nKilling)
                return -1;
            else
                return 0;
        }
        public CRecords() { }
        public CRecords(int _nId, string _sWeapon, string _sLocation, int _nKillings) 
        {
            nId = _nId;
            sWeapon = _sWeapon;
            sLocation = _sLocation;
            nKilling = _nKillings;
        }
    }
    public class CHunter
    {
        public int nId { get; set; }
        public string sName { get; set; }
    }
    class CContain
    {
        public List<CHunter> LHunters => new List<CHunter>
        {
            new CHunter{nId = 1, sName = "Andre"},
            new CHunter{nId = 2, sName = "Taras"},
            new CHunter{nId = 3, sName = "Stephan"},
            new CHunter{nId = 4, sName = "Vlad"},
            new CHunter{nId = 5, sName = "Maks"}
        };
        public List<CRecords> LResult
        {
            get
            {
                return new List<CRecords>
                {
                    new CRecords{nId = 4, sWeapon = "Gun", sLocation = "Jungle", nKilling = 3 },
                    new CRecords{nId = 1, sWeapon = "Gun", sLocation = "Forest", nKilling = 8 },
                    new CRecords{nId = 3, sWeapon = "Knife", sLocation = "Tropics", nKilling = 4 },
                    new CRecords{nId = 5, sWeapon = "Arbalet", sLocation = "Jungle", nKilling = 11 },
                    new CRecords{nId = 1, sWeapon = "Bow", sLocation = "Desert", nKilling = 3 },
                    new CRecords{nId = 2, sWeapon = "Gun", sLocation = "Forest", nKilling = 12 },
                    new CRecords{nId = 1, sWeapon = "Pistol", sLocation = "Forest", nKilling = 7 },
                    new CRecords{nId = 4, sWeapon = "Bow", sLocation = "Tropics", nKilling = 5 },
                    new CRecords{nId = 5, sWeapon = "Bow", sLocation = "Desert", nKilling = 3 },
                };
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var contain = new CContain();
            var hunters = new List<CHunter>(contain.LHunters);
            var results = new List<CRecords>(contain.LResult);
            Console.WriteLine("...............Hunters...............\n");
            Console.WriteLine("{0,-10}{1,-10}\n", "id", "name");
            foreach (var player in hunters)
                Console.WriteLine(String.Format("{0,-10}{1,-10}", player.nId, player.sName));
            Console.WriteLine("\n...............Results...............\n");
            Console.WriteLine("{0,-10}{1,-10}{2,-10}{3,-10}\n", "id", "weapon", "location", "killings");
            var vResults = from result in results select result;
            foreach (CRecords result in vResults)
                Console.WriteLine(String.Format("{0,-10}{1,-10}{2,-10}{3,-10}", result.nId, result.sWeapon, result.sLocation, result.nKilling));
            Console.WriteLine();
            int nTemporary = 0;

            //where
            Console.Write("Enter id of hunters to get details: ");
            nTemporary = Convert.ToInt32(Console.ReadLine());
            var vHunter = from result in results where result.nId == nTemporary select result;
            foreach (CRecords result in vHunter)
                Console.WriteLine(String.Format("\nHunter with id = {0} with weapon = {1} in location = {2} killed {3} animals.", result.nId, result.sWeapon, result.sLocation, result.nKilling));
            Console.WriteLine();

            //sorting
            Console.WriteLine("\n...............Sorting...............\n");
            Console.Write("Enter parametr of sorting:\n1 - weapon\n2 - location\n3 - killings\nYour choose: ");
            nTemporary = Convert.ToInt32(Console.ReadLine());
            var vSortResult = from result in results select result;
            if(nTemporary == 1)
                vSortResult = from result in results orderby result.sWeapon select result;
            else if(nTemporary == 2)
                vSortResult = from result in results orderby result.sLocation select result;
            else
                vSortResult = from result in results orderby result.nKilling select result;
            Console.WriteLine("Sorting result: ");
            Console.WriteLine("{0,-10}{1,-10}{2,-10}{3,-10}\n", "id", "weapon", "location", "killings");
            foreach (CRecords result in vSortResult)
                Console.WriteLine(String.Format("{0,-10}{1,-10}{2,-10}{3,-10}", result.nId, result.sWeapon, result.sLocation, result.nKilling));
            Console.WriteLine();

            //group by
            Console.WriteLine("How many huntings was attended by hunters");
            var vResultGroup = from result in results group result by result.nId into result_group select new { _name = result_group.Key, _count = result_group.Count() };
            foreach (var result in vResultGroup)
                Console.WriteLine(result._name + " hunter attend " + result._count + " huntings.");
            Console.WriteLine();

            //convert to array
            Console.WriteLine("\n...............ConvertToArray...............\n");
            var hunterArray = hunters.ToArray().OrderBy(hunter => hunter.nId);
            foreach (var hunter in hunterArray)
                Console.WriteLine(String.Format("id = {0,-10}name = {1,-10}", hunter.nId, hunter.sName));
            Console.WriteLine();

            //sorted List
            Console.WriteLine("\n...............SortedList...............\n");
            SortedList sortHunters = new SortedList();
            foreach (var player in hunters)
                sortHunters.Add(player.sName, player.nId);
            foreach (DictionaryEntry dd in sortHunters)
                Console.WriteLine(String.Format("Name = {0,-10}Id = {1,-10}", dd.Key, dd.Value));
            Console.WriteLine();

            //ANONYMOUS_CLASS
            Console.WriteLine("\n.............Anonymous_class.............\n");
            var anon_class = hunters.Select(i => new { new_id = i.nId, new_name = i.sName });
            foreach (var x in anon_class)
                Console.WriteLine(String.Format("Id: {0,-10}Name: {1,-10}", x.new_id, x.new_name));
            Console.WriteLine();

            //Dictionary 
            Console.WriteLine("\n...............Dictionary...............\n");
            Dictionary<string,int> dictionaryHunters = new Dictionary<string, int>(results.Capacity);
            foreach (var rec in results)
            {
                if (!dictionaryHunters.ContainsKey(rec.sLocation))
                    dictionaryHunters.Add(rec.sLocation, rec.nKilling);
                else
                {
                    dictionaryHunters.Remove(rec.sLocation);
                    dictionaryHunters.Add(rec.sLocation, rec.nKilling);
                }
            }
            foreach (KeyValuePair<string, int> keyValue in dictionaryHunters)
                Console.WriteLine(String.Format("In {0} were killed {1} animals.", keyValue.Key, keyValue.Value));

            Console.ReadLine();
        }
    }
}
