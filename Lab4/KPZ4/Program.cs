﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ4
{
    interface IAction
    {
        void vRun();
        void vShoot();
    }
    abstract class CHunter
    {
        public string sName { get; set; }
        public string sLocation { get; set; }
        public string sWeapon { get; set; }
        private int nPrivValue;
        public int nPublValue;
        protected int nProtValue;
        int nWithoutValue;
        public CHunter()
        {

        }
        public CHunter(int nValue)
        {

        }
        public CHunter(string _sName, string _sLocation, string _sWeapon)
        {
            sName = _sName;
            sLocation = _sLocation;
            sWeapon = _sWeapon;
            Console.WriteLine("Base constructor");
        }
        void vRun()
        {
            nProtValue = 1;
            nPublValue = 2;
            nPrivValue = 3;
            nWithoutValue = 4;
        }
        public abstract void vPlayGame();

    }
    enum Location
    {
        Desert, Western, Forest, Tropics
    };
    class CTypeHunter1 : CHunter, IAction
    {
        public int nId;
        public void vRun()
        {
            nProtValue = 1;
            nPublValue = 2;
            Console.WriteLine(sName + " is running.");
        }
        public void vShoot()
        {
            Console.WriteLine(sName + " is shoting by " + sWeapon);
        }
        public CTypeHunter1(int _nId)
        {
            nId = _nId;
        }
        public CTypeHunter1() : base("Yura", "Western", "Glock")
        {
            this.sName = "Oleg";
            Console.WriteLine("Child constructor");
        }
        public override void vPlayGame()
        {
            Console.WriteLine("Please, play game.");
        }
        //перевантаження функції
        public void vDisplay()
        {
            int nType = Convert.ToInt32(Location.Forest) & Convert.ToInt32(Location.Western);
            Console.WriteLine("TYPE = " + nType);
            int a = 1, b = 0;
            if (sName == "Yura" && sLocation == "Location" || sWeapon == "Glock")
                Console.WriteLine("Already is okay.");
            Console.WriteLine("Operator | = " + (a | b) + " Operator & = " + (a & b));
        }
        public void vDisplay(int n)
        {
            Console.WriteLine("Overriding function 1 + 1 = " + n);
        }
        public void vValue(int nCount)
        {
            nCount = 5;
        }
        public void vOUT(out int nCount)
        {
            //Console.WriteLine(nCount); тільки для ініціалізації
            nCount = 2;
        }
        public void vREF(ref int nCount)
        {
            nCount = 1;
        }
        public void vBoxing()
        {
            int a = 0;
            Console.WriteLine("int = " + a);
            Object obj = a;
            Console.WriteLine("object = " + obj);
        }
        public void vUnboxing()
        {
            Object obj = 5;
            Console.WriteLine("object = " + obj);
            int a = (int)obj;
            Console.WriteLine("int = " + a);
        }

        public static implicit operator int(CTypeHunter1 obj)
        {
            return obj.nId;
        }
        public static implicit operator CTypeHunter1(int nValue)
        {
            return new CTypeHunter1(nValue);
        }

    }
    class CNewHunter
    {
        static int nType;
        static readonly double dNUMBERPi = 3.14;
        static CNewHunter()
        {
            nType = 2;
            Console.WriteLine("Number pi = " + dNUMBERPi);
        }
        public int nValue { get; set; }
        public CNewHunter()
        {
            ++nType;
            Console.WriteLine("Type = " + nType);
        }
        public CNewHunter(int _nValue)
        {
            ++nType;
            Console.WriteLine("Type = " + nType);
            nValue = _nValue;
        }
        public static explicit operator CLearner(CNewHunter obj)
        {
            return new CLearner(obj.nValue);
        }
    }
    internal class CLearner : CTypeHunter1
    {
        public CLearner()
        {

        }
        public CLearner(int _nID)
        {
            nId = _nID;
        }
        public static explicit operator CNewHunter(CLearner obj)
        {
            return new CNewHunter(obj.nId);
        }
        private void func1()
        {
            Console.WriteLine("Learner1 is styding.");
        }
        public void func2()
        {
            Console.WriteLine("Learner2 is styding.");
        }
        protected void func3()
        {
            Console.WriteLine("Learner3 is styding.");
        }
        internal void func4()
        {
            Console.WriteLine("Learner4 is styding.");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //task 1
            var childobj = new CTypeHunter1();
            childobj.vRun();
            childobj.vShoot();
            //task 2 
            childobj.nPublValue = 2;
            //task 4
            var obj1 = new CLearner();
            obj1.func2();
            obj1.func4();
            //task5
            var hunter1 = new CTypeHunter1();
            hunter1.vDisplay();
            hunter1.vDisplay(2);
            Console.WriteLine(".......Passing.......");
            int a = 3;
            hunter1.vValue(a);
            Console.WriteLine("By value = " + a);
            hunter1.vOUT(out a);
            Console.WriteLine("By out = " + a);
            hunter1.vREF(ref a);
            Console.WriteLine("By reference = " + a);
            Console.WriteLine(".......Boxing.......");
            hunter1.vBoxing();
            hunter1.vUnboxing();
            Console.WriteLine(".......Implicit.......");
            CTypeHunter1 n1 = new CTypeHunter1(5);
            int nValue = n1;
            nValue = 5;
            n1 = nValue;
            Console.WriteLine("Result implicit = " + n1.nId);
            Console.WriteLine(".......Explicit.......");
            CNewHunter NH = new CNewHunter(10);
            CLearner NHL = (CLearner)NH;
            Console.WriteLine("Result explicit = " + NHL.nId);

            CNewHunter NH1 = new CNewHunter(10);
            
            Console.ReadLine();
        }
    }
}
