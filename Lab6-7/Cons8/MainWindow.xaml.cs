﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace Cons8
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataModel.DataModel dataModel = new DataModel.DataModel();

        public MainWindow()
        {
            InitializeComponent();

            dataModel = Client.Client.Load();
            mainDataGrid.ItemsSource = dataModel.characters;
        }
        
        private void button_Click(object sender, RoutedEventArgs e)
        {
            Client.Client.Clean();
            foreach (DataModel.Character character in dataModel.characters)
            {
                Client.Client.AddInfo(character);
            }
        }
    }
}
