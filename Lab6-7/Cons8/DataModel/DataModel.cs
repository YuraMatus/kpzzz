﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.DataModel
{
    public class DataModel
    {
        public DataModel()
        {
            characters = new ObservableCollection<Character>();
        }

        public ObservableCollection<Character> characters { set; get; }
    }
}
