﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.DataModel
{
    public class Character
    {
        public Character() { }
        public Character(Character character)
        {
            name = character.name;
            hp = character.hp;
            mp = character.mp;
            owner = character.owner;
        }

        public string name { set; get; }
        public int hp { set; get; }
        public int mp { set; get; }
        public string owner { set; get; }
    }
}
