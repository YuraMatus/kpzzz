﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.DataContract
{
    [DataContract]
    public class Character
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int HP { get; set; }

        [DataMember]
        public int MP { get; set; }

        [DataMember]
        public string Owner { get; set; }
    }
}
