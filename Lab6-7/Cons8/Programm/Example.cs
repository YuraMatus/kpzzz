﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.Programm
{
    class Example
    {
        private static void ShowExample(string[] args)
        {
            try
            {
                var service = new Service.DBService();
                DataContract.SystemMEssage m = service.Login();
                Console.WriteLine(m.Message);
                service.Logout();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
