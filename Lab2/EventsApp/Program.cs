﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var smth = new Smth();

            Console.WriteLine("Can I send something?");
            var result = Console.ReadKey().KeyChar;
            Console.WriteLine();

            if (result == 'yes')
            {
                smth.send();
            }
            else
            {
                smth.notSend();
            }

            Console.ReadLine();
        }
    }
}
