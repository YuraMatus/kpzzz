﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsApp
{
    class Smth
    {
        public event stateHandler OnCreate;

        public event stateHandler WithAdditionalInfo;

        public event stateHandler Without;

        public delegate void stateHandler();

        public Smth()
        {
            OnCreate += SaySmth;
            WithAdditionalInfo += SaySmth2Times;
            Without += SaySmthLoudly;
            OnCreate();
        }

        private void SaySmth()
        {
            Console.WriteLine("Smth");
        }

        private void SaySmth2Times()
        {
            Console.WriteLine("Smth, Smth");
        }

        private void SaySmthLoudly()
        {
            Console.WriteLine("Smth!!!");
        }

        public void send()
        {
            WithAdditionalInfo();
        }

        public void notSend()
        {
            Without();
        }

    }
}
